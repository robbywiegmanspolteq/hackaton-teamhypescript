import { by, element, ElementFinder, protractor } from 'protractor';

export class AuthenticationPage {

  emailTextfield: ElementFinder = element(by.css('input#email'));
  passwordTextfield: ElementFinder = element(by.css('input#passwd'));
  loginButton: ElementFinder = element(by.css('button#SubmitLogin'));
  invalidEmail: ElementFinder = element(by.css('.form-group.form-error #email'));
  newUserMail: ElementFinder = element(by.name('[for=\'email_create\'] + [type=\'email\']'));
  createNewUserButton: ElementFinder = element(by.css('button#SubmitLogin'));

  async setNewUserMail(mail: string) {
    await this.newUserMail.sendKeys(mail);
    await this.newUserMail.sendKeys(protractor.Key.TAB);
  }

  async clickCreateNewUser() {
    await this.createNewUserButton.click();
  }

  async isNewUserMailValid(): Promise<boolean> {
    const okElements = element.all(by.css('[id=\'create-account_form\'] [class=\'form-group form-ok\']'));
    const errorElements = element.all(by.css('[id=\'create-account_form\'] [class=\'form-group form-error\']'));
    if (await okElements.count() !== 0) {
      return true;
    }
    if (await errorElements.count() !== 0) {
      return false;
    }
    else {
      return false;
    }
  }

  async login(email: string, password: string) {
    await this.emailTextfield.sendKeys(email);
    await this.passwordTextfield.sendKeys(password);
    await this.loginButton.click();
  }

}
