import {browser, by, element, ElementFinder} from 'protractor';

export class HomePage {

  commentName: ElementFinder = element(by.id('name'));
  commentEmail: ElementFinder = element(by.id('email'));
  commentPhone: ElementFinder = element(by.id('phone'));
  commentSubject: ElementFinder = element(by.id('subject'));
  commentMessage: ElementFinder = element(by.id('description'));
  commentSubmit: ElementFinder = element(by.id('submitContact'));
  // commentResponseMessage: ElementFinder = element(by.xpath('//*[contains(text(), "Thanks for getting in touch")]'));

  async submitMessage() {
// TODO scroll to this place
    await this.commentName.sendKeys('naam');
    await this.commentEmail.sendKeys('email@email.com');
    await this.commentPhone.sendKeys('12345678901');
    await this.commentSubject.sendKeys('Vraag');
    await this.commentMessage.sendKeys('Dit is mijn vraag voor jullie');
    await this.commentSubmit.click();
  }

  async getResponseMessage(): Promise<string> {
    await browser.sleep(1000);
    const commentResponseMessage: ElementFinder = element(by.xpath('//*[contains(text(), "Thanks for getting in touch")]'));
    return await commentResponseMessage.getText();
  }
}
