Feature: Example keestest
  As Kees, I want to do opoen de test website

  Scenario: Example non angular website
    Given I am on the Polteq Webshop
    And I login with username test@polteq.com and password secretpassword
    Then I expect to be logged in as Polteq Tester

  Scenario: Example API
    Given I search for Max Verstappen on the drivers endpoint of the Ergast API
    Then I expect a result containing Dutch
