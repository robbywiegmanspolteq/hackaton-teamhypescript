import { After, Before, setDefaultTimeout } from 'cucumber';
import { browser } from 'protractor';


setDefaultTimeout(90000);

Before(async function() {
  browser.waitForAngularEnabled(false);
  this.apiresponse = undefined;
  await browser.driver.get(browser.baseUrl)
  await browser.manage().addCookie({ name: 'welcome', value: 'true' });
});

After(function(testCase: any) {

  if (testCase.result.status === 'failed') {
  }
});
