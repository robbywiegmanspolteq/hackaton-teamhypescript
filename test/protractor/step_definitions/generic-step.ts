import {Given, Then, When} from 'cucumber';
import {browser} from 'protractor';
import {HomePage} from '../pageobjects/homePage';
import * as chai from 'chai';

chai.use(require('chai-string'));

const homePage = new HomePage();
const expect = chai.expect;

Given(/^I am on the booking shop$/, async function () {
  await browser.get(browser.baseUrl);
  await browser.sleep(5000);
});

When(/^I add a message$/, async function () {
  await homePage.submitMessage();
});

Then(/^I should get a confirmation$/, async function () {
  const response = await homePage.getResponseMessage();
  console.log(response);
  expect(response).to.contain('naam');
});
